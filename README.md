Overview
--------

This *vector* extension for [SQLite](https://sqlite.org/index.html) has functions for
retrieving 1-d arrays, here called vectors, of single and double precision floats and 8,
4, and 2 byte wide integers as well as single bytes which have been stored as raw binary
blobs. The indexing is 1-based.

The main motivation for creating this extension has been that a large amount of sensor
data were available in single precision. Storing the 4-byte floats as BLOBS nearly halfs
the disk space compared to using the built-in 8-byte REAL type. 

In this *vector* extension no info on the vector type is stored, just the raw bytes as a `BLOB`.

In this extension no endian-ness conversions are done. The data are stored with the
endian-ness of the CPU hardware, which is overwhelmingly little endian. An SQLite database
file with BLOBs representing 1-d vectors can be transfered as usual to other computers,
except in the rare cases when the CPU endian-ness do not match, see e.g. the discussion at

https://sqlite.org/forum/info/4b040f50c8ef8491f8a4a853f3c453397b6d9fab1ca10fe4f873dbd6a8ff484f

Functions
---------

The *vector* extension provides functions

- `float32(...)`
- `float64(...)`
- `int64(...)`
- `int32(...)`
- `int16(...)`
- `byte(...)`

If there are 0 arguments to these function, then `NULL` is returned.

If the 1st argument of any of these functions is a `BLOB` with more then 0 bytes, then the
2nd argument is interpreted as a 1-based index `idx`. If the `BLOB` is the only argument
then `idx=1`. The `BLOB` is interpreted as a 1-d raw binary vector of the types as
indicated by the function name, i.e. in C `float`, `double`, `int64_t`, `int32_t`,
`int16_t`, and `unsigned char`, respectively. The returned SQLite type is 

- `float32(b, idx)` --> `SQLITE_FLOAT`
- `float64(b, idx)` --> `SQLITE_FLOAT`
- `int64(b, idx)` --> `SQLITE_INT64`
- `int32(b, idx)` --> `SQLITE_INT`
- `int16(b, idx)` --> `SQLITE_INT`
- `byte(b, idx)` --> `SQLITE_INT`

Exceptions: if the index `idx` is out of range, i.e. `idx<1` or a data beyond the size
of the `b` is requested, or if the length of the `b` is not a multiple of the vector
element size, then the functions returns `NULL`.

If the 1st argument of the function is not a `BLOB` or is a 0-length `BLOB`, then a
`BLOB` of a vector of all arguments with C binary presentations of `float`, `double`,
`int64_t`, `int32_t`, `int16_t`, and `unsigned char`, respectively, is returned. A `NULL`
argument or a 0-length `BLOB` are converted to bit patterns of IEEE `NaN` (Not a
Number) or to `0`, for floating point or integer functions, respectively. The functions

- `nan()`
- `nan64()` (the same as `nan`)
- `nan32()`

return 8- or 4-byte blobs with NaN bit patterns and so provide more explicit ways to
insert NaNs into *vector*s. NaNs can be retrieved via the (C
API)[https://www.sqlite.org/c3ref/intro.html]. But note that the extension is not able to
persuade SQLite to use NaNs internally, i.e. pulling a NaN from a *vector* always becomes
`NULL`:

`SELECT float32(nan32());` --> `NULL`

The functions

- `endian()` --> 'little' or 'big'
- `bigend()` --> 1 on big-endian machines, 0 otherwise

could be used to indicate the endian-ness of a database with *vector*s by inserting the
results into a special table, e.g.

`CREATE TABLE bigend(e);`
`INSERT INTO bigend VALUES(bigend());`

Applications can so determine whether endian-ness needs to be converted.

Examples:
---------

    SELECT float32(x'00038045');               --> 4096.375
    SELECT float32(x'0000004000038045', 1);    --> 2.0 
    SELECT float32(x'0000004000038045', 2);    --> 4096.375

    SELECT hex(float32(4096.375, '2', 3.14, nan32(), x'')); -->
    0003804500000040C3F548400000C07F0000C07F

    SELECT float32(float32(4096.375, '2', 3.14, nan32(), x'')); --> 4096.375
    SELECT float32(float32(4096.375, '2a', 3.14, nan32(), x''), 2); --> 2
    SELECT float32(float32(4096.375, '2a', 3.14, nan32(), x''), 5); --> NULL

    SELECT byte(byte(256));  --> 0, integers wrap around unsigned

    SELECT int64(float64(1)); --> 4607182418800017408, misuse!

These examples provide a quick check whether the *vector* extension is correctly working.
Normally the *vector* BLOB would rarely be created with the CLI, but rather by an
application via the C API or a wrapper thereof. `test_vector.c` has examples how to insert
and retrieve vectors in C.

Related:
--------

This extension for 1-d arrays, especially for single precision floats, minimizes storage and
processing overhead. If complex data structures need to be stored in single columns, and
not minimizing disk space, rather completeness and flexibility are the priorities, then
perhaps have a look at the [JSON](https://sqlite.org/json1.html) extension.

Another array extension for SQLite is [here](https://github.com/nalgeon/sqlean).

