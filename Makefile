CC=gcc -O2

vector.so: vector.c
	$(CC) -fPIC -shared vector.c -o vector.so

test_vector: test_vector.c
	$(CC) test_vector.c -o test_vector -lsqlite3
