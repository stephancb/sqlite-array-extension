/*
** 2022-08-27
**
** The author disclaims copyright to this source code.  In place of
** a legal notice, here is a blessing:
**
**    May you do good and not evil.
**    May you find forgiveness for yourself and forgive others.
**    May you share freely, never taking more than you give.
**
******************************************************************************
* 
* This SQLite extension has functions for storing and retrieving 1-d arrays
* of single and double precision floats and 8, 4, and 2 byte wide integers as well as
* single bytes which are stored as raw binary blobs. The indexing is 1-based.
* 
* The main motivation for creating the extension was that a large amount of sensor data
* were available in single precision. Storing the 4-byte floats as BLOBS nearly halfs 
* the disk space compared to using the built-in 8-byte REAL type. No endian-ness conversion
* is done. 
* 
* In the CLI Load with 
* 
* .load pathtoextensions/array
* 
* Examples:
* 
* SELECT float32(x'00038045');               -> 4096.375
* SELECT float32(x'0000004000038045', 1);    -> 2.0 
* SELECT float32(x'0000004000038045', 2);    -> 4096.375
*/

#include "sqlite3ext.h"
SQLITE_EXTENSION_INIT1

#include <stdlib.h>
#include <string.h>

static void nbytes( sqlite3_context *context, int argc, sqlite3_value **argv) {
    sqlite3_result_int(context, sqlite3_value_bytes(argv[0]));
}

static void nan64( sqlite3_context *context, int argc, sqlite3_value **argv) {
    double a=strtod("NaN", NULL);
    sqlite3_result_blob(context, &a, 8, SQLITE_TRANSIENT);
}

static void nan32( sqlite3_context *context, int argc, sqlite3_value **argv) {
    float a=strtof("NaN", NULL);
    sqlite3_result_blob(context, &a, 4, SQLITE_TRANSIENT);
}

static void endian( sqlite3_context *context, int argc, sqlite3_value **argv) {
    static char *little="little",*big="big";
    unsigned int i = 1;
    char *c = (char*)&i;
    if( *c )
        sqlite3_result_text(context, little, -1, SQLITE_STATIC);
    else
        sqlite3_result_text(context, big, -1, SQLITE_STATIC);
}

static void bigend( sqlite3_context *context, int argc, sqlite3_value **argv) {
    unsigned int i = 1;
    char *c = (char*)&i;
    sqlite3_result_int(context, !(*c));
}

static void float32func( sqlite3_context *context, int argc, sqlite3_value **argv) {
    float a=0.0;
    int   idx,k,nbytes;
    char *b;
    
    if( argc>0 && sqlite3_value_type(argv[0])==SQLITE_BLOB
               && sqlite3_value_bytes(argv[0] )>0) {
        nbytes = sqlite3_value_bytes(argv[0]);
        if( (nbytes%4)!=0 )
            return sqlite3_result_null(context);

        if( argc>1 )
            idx = sqlite3_value_int(argv[1]);
        else
            idx = 1;

        if( idx<1 || idx*4>nbytes )
            return sqlite3_result_null(context);

        memcpy(&a, sqlite3_value_blob(argv[0]) + (idx-1)*4, 4);
        return sqlite3_result_double(context, (double) a);
    } else if( argc>0 ) {
        b = sqlite3_malloc(argc*4);
        for( k=0; k<argc; k++ ) {
            if( sqlite3_value_type(argv[k])==SQLITE_FLOAT || sqlite3_value_type(argv[k])==SQLITE_TEXT )
                a = sqlite3_value_double(argv[k]);
            else if( sqlite3_value_type(argv[k])==SQLITE_INTEGER )
                a = sqlite3_value_int64(argv[k]);
            else if( sqlite3_value_type(argv[k])==SQLITE_BLOB && sqlite3_value_bytes(argv[k])>=4 )
                memcpy(&a, sqlite3_value_blob(argv[k]), 4);
            else
                a = strtof("NaN", NULL);
            memcpy(b+k*4, &a, 4);
        }
        sqlite3_result_blob(context, b, argc*4, SQLITE_TRANSIENT);
        sqlite3_free(b);
    } else sqlite3_result_null(context);
}

static void float64func( sqlite3_context *context, int argc, sqlite3_value **argv ) {
    double a=0.0;
    int   idx,k,nbytes;
    char *b;
    
    if( argc>0 && sqlite3_value_type(argv[0])==SQLITE_BLOB
               && sqlite3_value_bytes(argv[0])>0 ) {
        nbytes = sqlite3_value_bytes(argv[0]);
        if( (nbytes%8)!=0 )
            return sqlite3_result_null(context);

        if( argc>1 )
            idx = sqlite3_value_int(argv[1]);
        else
            idx = 1;

        if( idx<1 || idx*8>nbytes )
            return sqlite3_result_null(context);
    
        sqlite3_result_double(context, *((double*) sqlite3_value_blob(argv[0]) + (idx-1)));
    } else if( argc>0 ) {
        b = sqlite3_malloc(argc*8);
        for( k=0; k<argc; k++ ) {
            if( sqlite3_value_type(argv[k])==SQLITE_FLOAT || sqlite3_value_type(argv[k])==SQLITE_TEXT )
                a = sqlite3_value_double(argv[k]);
            else if( sqlite3_value_type(argv[k])==SQLITE_INTEGER )
                a = sqlite3_value_int64(argv[k]);
            else if( sqlite3_value_type(argv[k])==SQLITE_BLOB && sqlite3_value_bytes(argv[k])>=8 )
                memcpy(&a, sqlite3_value_blob(argv[k]), 8);
            else
                a = strtod("NaN", NULL);
            memcpy(b+k*8, &a, 8);
        }
        sqlite3_result_blob(context, b, argc*8, SQLITE_TRANSIENT);
        sqlite3_free(b);
    } else sqlite3_result_null(context);
}

static void int64func( sqlite3_context *context, int argc, sqlite3_value **argv) {
    /* long int a=0; */
    int64_t a=0;
    int     idx,k,nbytes;
    unsigned char *b;

    if( argc>0 && sqlite3_value_type(argv[0])==SQLITE_BLOB
               && sqlite3_value_bytes(argv[0])>0 ) {
        nbytes = sqlite3_value_bytes(argv[0]);
        if( (nbytes%8)!=0 )
            return sqlite3_result_null(context);

        if( argc>1 )
            idx = sqlite3_value_int(argv[1]);
        else
            idx = 1;
        if( idx<1 || idx*8>nbytes )
            return sqlite3_result_null(context);

        sqlite3_result_int64(context, *((sqlite3_int64*) sqlite3_value_blob(argv[0]) + (idx-1)));
    } else if( argc>0 ) {
        b = sqlite3_malloc(argc*8);
        for( k=0; k<argc; k++ ) {
            if( sqlite3_value_type(argv[k])==SQLITE_FLOAT )
                a = sqlite3_value_double(argv[k]);
            else if(   sqlite3_value_type(argv[k])==SQLITE_INTEGER
                    || sqlite3_value_type(argv[k])==SQLITE_TEXT )
                a = sqlite3_value_int64(argv[k]);
            else if( sqlite3_value_type(argv[k])==SQLITE_BLOB && sqlite3_value_bytes(argv[k])>=8 )
                memcpy(&a, sqlite3_value_blob(argv[k]), 8);
            else
                a = 0;
            memcpy(b+k*8, &a, 8);
        }
        sqlite3_result_blob(context, b, argc*8, SQLITE_TRANSIENT);
        sqlite3_free(b);
    } else sqlite3_result_null(context);
}

static void int32func( sqlite3_context *context, int argc, sqlite3_value **argv) {
    int32_t a;
    int     idx,k,nbytes;
    unsigned char *b;

    if( argc>0 && sqlite3_value_type(argv[0])==SQLITE_BLOB
               && sqlite3_value_bytes(argv[0])>0 ) {
        nbytes = sqlite3_value_bytes(argv[0]);
        if( nbytes==0 || (nbytes%4)!=0 )
            return sqlite3_result_null(context);

        if( argc>1 )
            idx = sqlite3_value_int(argv[1]);
        else
            idx = 1;
        if( idx<1 || idx*4>nbytes )
            return sqlite3_result_null(context);
    
        sqlite3_result_int(context, *((int*) sqlite3_value_blob(argv[0]) + (idx-1)));
    } else if( argc>0 ) {
        b = sqlite3_malloc(argc*4);
        for( k=0; k<argc; k++ ) {
            if( sqlite3_value_type(argv[k])==SQLITE_FLOAT )
                a = sqlite3_value_double(argv[k]);
            else if(   sqlite3_value_type(argv[k])==SQLITE_INTEGER
                    || sqlite3_value_type(argv[k])==SQLITE_TEXT )
                a = sqlite3_value_int(argv[k]);
            else if( sqlite3_value_type(argv[k])==SQLITE_BLOB && sqlite3_value_bytes(argv[k])>=4 )
                memcpy(&a, sqlite3_value_blob(argv[k]), 4);
            else
                a = 0;
            memcpy(b+k*4, &a, 4);
        }
        sqlite3_result_blob(context, b, argc*4, SQLITE_TRANSIENT);
        sqlite3_free(b);
    } else sqlite3_result_null(context);
}

static void int16func( sqlite3_context *context, int argc, sqlite3_value **argv) {
    int16_t a=0;
    int     idx,k,nbytes;
    unsigned char *b;

    if( argc>0 && sqlite3_value_type(argv[0])==SQLITE_BLOB
               && sqlite3_value_bytes(argv[0])>0 ) {
        nbytes = sqlite3_value_bytes(argv[0]);
        if( nbytes==0 || (nbytes%2)!=0 )
            return sqlite3_result_null(context);

        if( argc>1 )
            idx = sqlite3_value_int(argv[1]);
        else
            idx = 1;
        if( idx<1 || idx*2>nbytes )
            return sqlite3_result_null(context);
    
        sqlite3_result_int(context, *((short int*) sqlite3_value_blob(argv[0]) + (idx-1)));
    } else if( argc>0 ) {
        b = sqlite3_malloc(argc*2);
        for( k=0; k<argc; k++ ) {
            if( sqlite3_value_type(argv[k])==SQLITE_FLOAT )
                a = sqlite3_value_double(argv[k]);
            else if(   sqlite3_value_type(argv[k])==SQLITE_INTEGER
                    || sqlite3_value_type(argv[k])==SQLITE_TEXT )
                a = sqlite3_value_int(argv[k]);
            else if( sqlite3_value_type(argv[k])==SQLITE_BLOB && sqlite3_value_bytes(argv[k])>=2 )
                memcpy(&a, sqlite3_value_blob(argv[k]), 2);
            else
                a = 0;
            memcpy(b+k*2, &a, 2);
        }
        sqlite3_result_blob(context, b, argc*2, SQLITE_TRANSIENT);
        sqlite3_free(b);
    } else sqlite3_result_null(context);
}

static void byte( sqlite3_context *context, int argc, sqlite3_value **argv) {
    unsigned char a=0;
    int           idx,k,nbytes;
    unsigned char *b;

    if( argc>0 && sqlite3_value_type(argv[0])==SQLITE_BLOB
               && sqlite3_value_bytes(argv[0])>0 ) {
        nbytes = sqlite3_value_bytes(argv[0]);
        if( nbytes==0 )
            return sqlite3_result_null(context);

        if( argc>1 )
            idx = sqlite3_value_int(argv[1]);
        else
            idx = 1;
        if( idx<1 || idx>nbytes )
            return sqlite3_result_null(context);
    
        sqlite3_result_int(context, *((unsigned char*) sqlite3_value_blob(argv[0]) + (idx-1)));
    } else if( argc>0 ) {
        b = sqlite3_malloc(argc);
        for( k=0; k<argc; k++ ) {
            if( sqlite3_value_type(argv[k])==SQLITE_FLOAT )
                a = sqlite3_value_double(argv[k]);
            else if(   sqlite3_value_type(argv[k])==SQLITE_INTEGER
                    || sqlite3_value_type(argv[k])==SQLITE_TEXT )
                a = sqlite3_value_int(argv[k]);
            else if( sqlite3_value_type(argv[k])==SQLITE_BLOB && sqlite3_value_bytes(argv[k])>=1 )
                memcpy(&a, sqlite3_value_blob(argv[k]), 1);
            else
                a = 0;
            memcpy(b+k, &a, 1);
        }
        sqlite3_result_blob(context, b, argc, SQLITE_TRANSIENT);
        sqlite3_free(b);
    } else sqlite3_result_null(context);
}

#ifdef _WIN32
__declspec(dllexport)
#endif
int sqlite3_vector_init( /* <== Change this name, maybe */
        sqlite3 *db, 
        char **pzErrMsg, 
        const sqlite3_api_routines *pApi
        ){
    int rc = SQLITE_OK;
    SQLITE_EXTENSION_INIT2(pApi);
    /* insert code to initialize your extension here */
    rc = sqlite3_create_function(
            db, "nbytes", 1, SQLITE_UTF8 | SQLITE_DETERMINISTIC | SQLITE_INNOCUOUS,
            0 /* no user data */, nbytes, 0, 0);
    rc = sqlite3_create_function(
            db, "nan", 0, SQLITE_UTF8 | SQLITE_DETERMINISTIC | SQLITE_INNOCUOUS,
            0 /* no user data */, nan64, 0, 0);
    rc = sqlite3_create_function(
            db, "nan64", 0, SQLITE_UTF8 | SQLITE_DETERMINISTIC | SQLITE_INNOCUOUS,
            0 /* no user data */, nan64, 0, 0);
    rc = sqlite3_create_function(
            db, "nan32", 0, SQLITE_UTF8 | SQLITE_DETERMINISTIC | SQLITE_INNOCUOUS,
            0 /* no user data */, nan32, 0, 0);
    rc = sqlite3_create_function(
            db, "endian", 0, SQLITE_UTF8 | SQLITE_DETERMINISTIC | SQLITE_INNOCUOUS,
            0 /* no user data */, endian, 0, 0);
    rc = sqlite3_create_function(
            db, "bigend", 0, SQLITE_UTF8 | SQLITE_DETERMINISTIC | SQLITE_INNOCUOUS,
            0 /* no user data */, bigend, 0, 0);
    rc = sqlite3_create_function(
            db, "float32",-1, SQLITE_UTF8 | SQLITE_DETERMINISTIC | SQLITE_INNOCUOUS,
            0 /* no user data */, float32func, 0, 0);
    rc = sqlite3_create_function(
            db, "float64",-1, SQLITE_UTF8 | SQLITE_DETERMINISTIC | SQLITE_INNOCUOUS,
            0 /* no user data */, float64func, 0, 0);
    rc = sqlite3_create_function(
            db, "int64",-1, SQLITE_UTF8 | SQLITE_DETERMINISTIC | SQLITE_INNOCUOUS,
            0 /* no user data */, int64func, 0, 0);
    rc = sqlite3_create_function(
            db, "int32",-1, SQLITE_UTF8 | SQLITE_DETERMINISTIC | SQLITE_INNOCUOUS,
            0 /* no user data */, int32func, 0, 0);
    rc = sqlite3_create_function(
            db, "int16",-1, SQLITE_UTF8 | SQLITE_DETERMINISTIC | SQLITE_INNOCUOUS,
            0 /* no user data */, int16func, 0, 0);
    rc = sqlite3_create_function(
            db, "byte",-1, SQLITE_UTF8 | SQLITE_DETERMINISTIC | SQLITE_INNOCUOUS,
            0 /* no user data */, byte, 0, 0);
    return rc;
}

