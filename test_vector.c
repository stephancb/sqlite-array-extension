/*
** 2022-09-14
**
** The author disclaims copyright to this source code.  In place of
** a legal notice, here is a blessing:
**
**    May you do good and not evil.
**    May you find forgiveness for yourself and forgive others.
**    May you share freely, never taking more than you give.
**
******************************************************************************
* 
* This main program tests several aspects of my 1-d array extension for SQlite.
*/
#include <stdio.h>
#include <string.h>
#include <sqlite3.h>

int exec_callback(void *, int argc, char **argv, char **columns){
    for (int i=0; i<argc; i++) {
        printf("%s", argv[i]);
        if( i<argc-1 ) printf("|");
    }
    printf("\n");
    return 0;
}

int main ( int argc, char **argv ) {
    sqlite3 *db;
    int iret,rp;
    char *vfs=NULL,*errmsg;
    const char *inssql = "INSERT INTO v VALUES(?1);";
    const char *exesql = "SELECT float32(x) FROM v;";
    const char *selsql = "SELECT x FROM v;";
    float x = 4096.375,*fp; 
    sqlite3_stmt *insstmt,*selstmt;

    iret = sqlite3_open_v2("", &db, SQLITE_OPEN_READWRITE|SQLITE_OPEN_MEMORY, vfs);
    if( iret!=SQLITE_OK ) {
        printf("%s\n", sqlite3_errmsg(db));
        return iret;
    }
    iret = sqlite3_db_config(db, SQLITE_DBCONFIG_ENABLE_LOAD_EXTENSION, 1, &rp);
    if( iret!=SQLITE_OK ) {
        printf("%s\n", sqlite3_errmsg(db));
        return iret;
    }
    if( rp==1 )
        printf("extension loading enabled\n");
    iret = sqlite3_load_extension(db, "./array.so", NULL, &errmsg);
    if( iret!=SQLITE_OK ) {
        printf("%s\n", errmsg);
        sqlite3_free(errmsg);
        return iret;
    } else
        printf("array extension loaded.\n");
    iret = sqlite3_exec(db, "CREATE TABLE v(x);", NULL, NULL, &errmsg);
    if( iret!=SQLITE_OK ) {
        printf("%s\n", sqlite3_errmsg(db));
        return iret;
    }
    iret = sqlite3_prepare_v2(db, inssql, -1, &insstmt, NULL);
    if( iret!=SQLITE_OK ) {
        printf("%s\n", sqlite3_errmsg(db));
        return iret;
    }
    iret = sqlite3_bind_blob(insstmt, 1, &x, 4, SQLITE_TRANSIENT);
    if( iret!=SQLITE_OK ) {
        printf("%s\n", sqlite3_errmsg(db));
        return iret;
    }
    iret = sqlite3_step(insstmt);
    if( iret!=SQLITE_DONE ) {
        printf("%s\n", sqlite3_errmsg(db));
        return iret;
    }
    iret = sqlite3_reset(insstmt);
    if( iret!=SQLITE_OK ) {
        printf("%s\n", sqlite3_errmsg(db));
        return iret;
    }

    printf("EXEC \"%s\": -> ", exesql);
    iret = sqlite3_exec(db, exesql, exec_callback, NULL, &errmsg);
    if( iret!=SQLITE_OK ) {
        printf("%s\n", sqlite3_errmsg(db));
        return iret;
    }

    iret = sqlite3_prepare_v2(db, selsql, -1, &selstmt, NULL);
    if( iret!=SQLITE_OK ) {
        printf("%s\n", sqlite3_errmsg(db));
        return iret;
    }
    iret = sqlite3_step(selstmt);
    if( iret!=SQLITE_ROW ) {
        printf("%s\n", sqlite3_errmsg(db));
        return iret;
    }
    if( sqlite3_column_type(selstmt, 0)!=SQLITE_BLOB )
        printf("wrong column type %d, expected is %d\n", sqlite3_column_type(selstmt, 0), SQLITE_BLOB);
    if( sqlite3_column_bytes(selstmt, 0)!=4 )
        printf("wrong nr of bytes %d, expected are 4 bytes\n", sqlite3_column_bytes(selstmt, 0));
    else {
        fp = (float*) sqlite3_column_blob(selstmt, 0);
        printf("C API \"%s\":         -> %f\n", selsql, *fp);
    }

    return sqlite3_close_v2(db);
}
